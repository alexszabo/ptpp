public class Graph {
    int N;
    int start;
    int end;
    int cost;
    String path;
    public int[][] adjMatrix;

    public Graph(int n, int start, int end) {
        N = n;
        this.start = start;
        this.end = end;
        adjMatrix = new int[N][N];
    }

    public int getN() {
        return N;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public void setN(int n) {
        N = n;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
