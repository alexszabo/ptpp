import java.io.*;

public class Main {
    static String source = "in2.txt";
    static String destination = "out.txt";

    public static Graph readGraph(){
        int N, start, end;
        Graph graph = null;
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(source));
            String line;
            String[] lineElems = br.readLine().split(" ");
            N = Integer.parseInt(lineElems[0]);
            start = Integer.parseInt(lineElems[1]);
            end = Integer.parseInt(lineElems[2]);
            graph = new Graph(N, start, end);
            int rowIndex = 0;
            while ((line = br.readLine()) != null) {
                lineElems = line.split(" ");
                for (int colIndex = 0; colIndex < N; colIndex++)
                    if (lineElems[colIndex].equals("Inf"))
                        graph.adjMatrix[rowIndex][colIndex] = Integer.MAX_VALUE;
                    else graph.adjMatrix[rowIndex][colIndex] = Integer.parseInt(lineElems[colIndex]);
                rowIndex++;
            }
            br.close();
        } catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
        return graph;
    }

    public static void writeSolution(Graph graph) {
        BufferedWriter bw;
        try{
            bw = new BufferedWriter(new FileWriter(destination));
            if (graph.getCost() == Integer.MAX_VALUE)
                bw.write(String.format("There is no available path from %d to %d.", graph.getStart(), graph.getEnd()));
            else {
                bw.write(String.format("Shortest path from %d to %d, with a cost of %d is:", graph.getStart(), graph.getEnd(), graph.getCost()));
                bw.newLine();
                bw.write(graph.getPath());
            }
            bw.close();
        } catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Graph graph = readGraph();
        long start, end;
        if (graph == null) {
            System.err.println("Error!");
            return;
        }
        start = System.nanoTime();
        Implementation.parallelImp(graph);
        end = System.nanoTime();
        System.out.println("Parallel Operation time: " + (end-start));
        start = System.nanoTime();
        Implementation.serialImp(graph);
        end = System.nanoTime();
        System.out.println("Serial Operation time: " + (end-start));
        writeSolution(graph);
    }
}
