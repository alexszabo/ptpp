public class Implementation {
    private static Integer[][] cost, path;

    private static String createPathString(Integer[][] path, int u, int v)
    {
        if (path[u][v] == -1){
            return "No path is available";
        }
        String pathString = u + " ";
        while (v != u){
            u = path[u][v];
            pathString += (u + " ");
        }
        return pathString;
    }

    private static void initialize(Graph graph){
        int N = graph.getN();
        // cost[][] and path[][] store shortest path (shortest-cost/shortest route) information
        cost = new Integer[N][N];
        path = new Integer[N][N];

        // initialize cost and path matrices
        for (int v = 0; v < N; v++)
        {
            for (int u = 0; u < N; u++)
            {
                // initial cost is the same as the weight of the edge
                cost[v][u] = graph.adjMatrix[v][u];

                if (v == u)
                    path[v][u] = v;
                else if (cost[v][u] != Integer.MAX_VALUE)
                    path[v][u] = u;
                else
                    path[v][u] = -1;
            }
        }
    }

    // Function to run Floyd-Warshall algorithm serially
    public static void serialImp(Graph graph)
    {
        int N  = graph.getN();
        initialize(graph);
        // run Floyd-Warshall
        for (int k = 0; k < N; k++)
        {
            for (int u = 0; u < N; u++)
            {
                for (int v = 0; v < N; v++)
                {
                    // If vertex k is on the shortest path from v to u,
                    // then update the value of cost[v][u], path[v][u]

                    if (cost[u][k] != Integer.MAX_VALUE
                            && cost[k][v] != Integer.MAX_VALUE
                            && (cost[u][k] + cost[k][v] < cost[u][v]))
                    {
                        cost[u][v] = cost[u][k] + cost[k][v];
                        path[u][v] = path[u][k];
                    }
                }
            }
        }

        // Save the shortest path between selected vertices
        graph.setPath(createPathString(path, graph.getStart(), graph.getEnd()));
        graph.setCost(cost[graph.getStart()][graph.getEnd()]);
        for(int i=0;i<N;i++){
            for(int j=0;j<N;j++){
                System.out.print(cost[i][j] + " ");
            }
            System.out.println();
        }
    }

    //Function to run Floyd-Warshall algorithm in parallel
    public static void parallelImp(Graph graph){
        int N  = graph.getN();
        int threadCounter = 0;
        GraphThread[] threadArray = new GraphThread[N*N];
        initialize(graph);
        // run Floyd-Warshall
        for (int k = 0; k < N; k++)
        {
            for (int u = 0; u < N; u++)
            {
                GraphThread gt = new GraphThread(cost, path, k, u, N);
                threadArray[threadCounter++] = gt;
                gt.start();
            }
        }

        for(GraphThread gt: threadArray){
            try{
                gt.join();
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }

        // Print the shortest path between selected vertices
        graph.setPath(createPathString(path, graph.getStart(), graph.getEnd()));
        graph.setCost(cost[graph.getStart()][graph.getEnd()]);
        for(int i=0;i<N;i++){
            for(int j=0;j<N;j++){
                System.out.print(cost[i][j] + " ");
            }
            System.out.println();
        }
    }
}
