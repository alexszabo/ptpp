public class GraphThread extends Thread {
    Integer[][] cost, path;
    int k,i, N;

    public GraphThread(Integer[][] cost, Integer[][] path, int k, int i, int n) {
        this.cost = cost;
        this.path = path;
        this.k = k;
        this.i = i;
        N = n;
    }

    @Override
    public void run() {
        for (int j = 0; j < N; j++) {
            synchronized (cost[i]) {
                if (cost[i][k] != Integer.MAX_VALUE
                        && cost[k][j] != Integer.MAX_VALUE
                        && (cost[i][k] + cost[k][j] < cost[i][j])) {
                    cost[i][j] = cost[i][k] + cost[k][j];
                    path[i][j] = path[i][k];
                }
            }
        }
    }
}

